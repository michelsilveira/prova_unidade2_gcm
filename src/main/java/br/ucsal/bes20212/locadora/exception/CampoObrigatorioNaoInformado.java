package br.ucsal.bes20212.locadora.exception;

public class CampoObrigatorioNaoInformado extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CampoObrigatorioNaoInformado(String campo) {
		super("Campo obrigat�rio "+campo+" n�o informado.");
	}

}
