package br.ucsal.bes20212.locadora.business;

import java.time.LocalDate;
import java.util.List;

import br.ucsal.bes20212.locadora.dominio.Veiculo;
import br.ucsal.bes20212.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.locadora.persistence.VeiculoDAO;

public class LocacaoBO {

	public VeiculoDAO veiculoDAO;

	public LocacaoBO(VeiculoDAO veiculoDAO) {
		this.veiculoDAO = veiculoDAO;
	}

	public Double calcularValorTotalLocacao(List<String> placas, Integer quantidadeDiasLocacao,
			LocalDate dataReferencia) throws VeiculoNaoEncontradoException {
		Double total = 0d;
		Double valorLocacaoVeiculo;
		Integer anoAtual = dataReferencia.getYear();
		List<Veiculo> veiculos = veiculoDAO.obterPorPlacas(placas);

		for (Veiculo veiculo : veiculos) {
			valorLocacaoVeiculo = veiculo.getValorDiaria() * quantidadeDiasLocacao;
			if (veiculo.getAnoFabricacao() < anoAtual - 5) {
				valorLocacaoVeiculo *= .9;
			}
			total += valorLocacaoVeiculo;
		}

		return total;
	}

}