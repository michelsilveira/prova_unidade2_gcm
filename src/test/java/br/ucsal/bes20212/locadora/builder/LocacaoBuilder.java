package br.ucsal.bes20212.locadora.builder;

import java.util.Date;
import java.util.List;

import br.ucsal.bes20212.locadora.dominio.Cliente;
import br.ucsal.bes20212.locadora.dominio.Locacao;
import br.ucsal.bes20212.locadora.dominio.Veiculo;

public class LocacaoBuilder {
	private Integer numeroContrato;
	
	private Cliente cliente;

	private List<Veiculo> veiculos;

	private Date dataLocacao;

	private Integer quantidadeDiasLocacao;

	private Date dataDevolucao;
	
	public LocacaoBuilder comNumeroContrato(Integer numContrato) {
		this.numeroContrato = numContrato;
		return this;
	}
	
	public LocacaoBuilder comCliente(Cliente cliente) { 
		this.cliente = cliente;
		return this;
	}
	
	public LocacaoBuilder comVeiculos(List<Veiculo> veiculos) { 
		this.veiculos = veiculos;
		return this;
	}
	
	public LocacaoBuilder comData(Date dataLocacao) { 
		this.dataLocacao = dataLocacao;
		return this;
	}
	
	public LocacaoBuilder comQuantidadeDiasLocacao(Integer quantidade) { 
		this.quantidadeDiasLocacao = quantidade;
		return this;
	}
	
	public LocacaoBuilder comDataDevolucao(Date devolucao) { 
		this.dataDevolucao = devolucao;
		return this;
	}
	
	public Locacao build() { 
		return new Locacao(this.cliente, this.veiculos, this.dataLocacao, this.quantidadeDiasLocacao);
	}
	
}
