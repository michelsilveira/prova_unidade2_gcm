package br.ucsal.bes20212.locadora.builder;

import br.ucsal.bes20212.locadora.dominio.Cliente;

public class ClienteBuilder {
	private String cpf;
	
	private String nome;
	
	private String telefone;
	
	public ClienteBuilder comCpf(String cpf) { 
		this.cpf = cpf;
		return this;
	}
	
	public ClienteBuilder comNome(String nome) { 
		this.nome = nome;
		return this;
	}
	
	public ClienteBuilder comTelefone(String telefone) { 
		this.telefone = telefone;
		return this;
	}
	
	public Cliente build() { 
		return new Cliente(this.cpf,this.nome,this.telefone);
	}
}
