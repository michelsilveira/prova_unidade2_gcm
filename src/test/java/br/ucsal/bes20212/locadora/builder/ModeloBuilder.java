package br.ucsal.bes20212.locadora.builder;

import br.ucsal.bes20212.locadora.dominio.Modelo;

public class ModeloBuilder {
	private String nome;

	public ModeloBuilder comNome(String nome) { 
		this.nome = nome;
		return this;
	}
	
	public Modelo build() {
		Modelo modelo = new Modelo(this.nome);
		return modelo;
	}	
	
}
