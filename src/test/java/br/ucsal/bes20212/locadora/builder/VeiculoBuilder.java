package br.ucsal.bes20212.locadora.builder;

import br.ucsal.bes20212.locadora.dominio.Modelo;
import br.ucsal.bes20212.locadora.dominio.Veiculo;
import br.ucsal.bes20212.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	private String placa;
	
	private Integer anoFabricacao;
	
	private Modelo modelo;
	
	private Double valorDiaria;
	
	private SituacaoVeiculoEnum situacao = SituacaoVeiculoEnum.DISPONIVEL;
		
	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	
	public VeiculoBuilder comAnoFabricacao(Integer ano) { 
		this.anoFabricacao = ano;
		return this;
	}
	
	public VeiculoBuilder comModelo(Modelo modelo) { 
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuilder comValorDiaria(Double valorDiaria) { 
		this.valorDiaria = valorDiaria;
		return this;
	}
	
	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) { 
		this.situacao = situacao;
		return this;
	}
	
	public Veiculo build() { 
		Veiculo veiculo = new Veiculo();
		veiculo.setAnoFabricacao(this.anoFabricacao);
		veiculo.setModelo(this.modelo);
		veiculo.setValorDiaria(this.valorDiaria);
		veiculo.setPlaca(placa);
		return veiculo;
	}
	
}
