package br.ucsal.bes20212.locadora;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20212.locadora.builder.ModeloBuilder;
import br.ucsal.bes20212.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20212.locadora.business.LocacaoBO;
import br.ucsal.bes20212.locadora.dominio.Modelo;
import br.ucsal.bes20212.locadora.dominio.Veiculo;
import br.ucsal.bes20212.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.locadora.persistence.VeiculoDAO;

public class LocacaoBOTest {

	static Veiculo veiculo1;
	static Veiculo veiculo2;
	static Veiculo veiculo3;
	static Veiculo veiculo4;
	static Veiculo veiculo5;
	static Veiculo veiculo6;

	static VeiculoBuilder veiculoBuilder = new VeiculoBuilder();
	static ModeloBuilder modeloBuilder = new ModeloBuilder();
	
	private static List<String> placas = new ArrayList<>();
	private static List<Veiculo> veiculos = new ArrayList<>();

	static VeiculoDAO veiculoDaoMock;
	static LocacaoBO locacaoHelper; 
	
	@BeforeAll 
	static void prep() { 
		veiculoDaoMock = Mockito.mock(VeiculoDAO.class);
		locacaoHelper = new LocacaoBO(veiculoDaoMock); 
		
		Modelo modelo = modeloBuilder.comNome("Carro").build();
		veiculo1 = veiculoBuilder.comAnoFabricacao(2019).comModelo(modelo).comPlaca("OLD1998").comValorDiaria(90d).build();
		veiculo2 = veiculoBuilder.comAnoFabricacao(2019).comModelo(modelo).comPlaca("OLD2004").comValorDiaria(50d).build();
		veiculo3 = veiculoBuilder.comAnoFabricacao(2012).comModelo(modelo).comPlaca("OLD2002").comValorDiaria(60d).build();
		veiculo4 = veiculoBuilder.comAnoFabricacao(2012).comModelo(modelo).comPlaca("OLD1998").comValorDiaria(90d).build();
		veiculo5 = veiculoBuilder.comAnoFabricacao(2012).comModelo(modelo).comPlaca("OLD2004").comValorDiaria(50d).build();
		
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);
		
		for (Veiculo veiculo : veiculos) {
			placas.add(veiculo.getPlaca());
		}
		
	}

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {
		
		Double valorEsperado;
		
		try {
			Mockito.when(veiculoDaoMock.obterPorPlacas(placas)).thenReturn(veiculos);
		} catch (VeiculoNaoEncontradoException e) {
			e.printStackTrace();
		}

		valorEsperado = 960.0d;
		assertEquals(valorEsperado, locacaoHelper.calcularValorTotalLocacao(placas, 3, LocalDate.of(2020,6,7)));
	}
}