package br.ucsal.bes20212.locadora;

import java.util.List;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20212.locadora.builder.ModeloBuilder;
import br.ucsal.bes20212.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20212.locadora.business.LocacaoBO;
import br.ucsal.bes20212.locadora.dominio.Modelo;
import br.ucsal.bes20212.locadora.dominio.Veiculo;
import br.ucsal.bes20212.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.locadora.persistence.VeiculoDAO;

public class LocacaoBOIntegradoTest {
	Veiculo veiculo;
	Veiculo veiculo2;
	Veiculo veiculo3;
	Modelo modelo;
	List<Veiculo> veiculos = new ArrayList<>();
	List<String> placas = new ArrayList<>();
	LocalDate data;
	
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws VeiculoNaoEncontradoException {
		VeiculoDAO testeDAO = new VeiculoDAO();
			
		for (Veiculo veiculo : veiculos) {
			placas.add(veiculo.getPlaca());
			testeDAO.insert(veiculo);
		}
		
		ModeloBuilder builder = new ModeloBuilder();
		modelo = builder.comNome("Carro").build();
		VeiculoBuilder veiculoBuilder = new VeiculoBuilder();
		
		veiculo = veiculoBuilder.comAnoFabricacao(1998).comModelo(modelo).comPlaca("OLD1998").comValorDiaria(90d).build();
		veiculo2 = veiculoBuilder.comAnoFabricacao(2000).comModelo(modelo).comPlaca("OLD2004").comValorDiaria(50d).build();
		veiculo3 = veiculoBuilder.comAnoFabricacao(2002).comModelo(modelo).comPlaca("OLD2002").comValorDiaria(60d).build();
		
		veiculos.add(veiculo);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		
		for (Veiculo veiculo : veiculos) {
			placas.add(veiculo.getPlaca());
			
			System.out.println(veiculo.getPlaca());
			testeDAO.insert(veiculo);
		}
	
		LocacaoBO testeBO = new LocacaoBO(testeDAO);
		
		Double esperado = 540d;
		
		Double valor = testeBO.calcularValorTotalLocacao(placas, 3, LocalDate.now());
		
		assertEquals(esperado, valor);
	
	}

}
